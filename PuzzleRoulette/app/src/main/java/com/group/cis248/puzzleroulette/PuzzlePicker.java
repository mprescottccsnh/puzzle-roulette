package com.group.cis248.puzzleroulette;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.security.SecureRandom;
import java.util.Random;


public class PuzzlePicker extends AppCompatActivity {

    private final Class<?>[] puzzles = { RandomNumberActivity_FG.class };
    SecureRandom random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puzzle_picker);
    }

    public void pickRandomPuzzle(View view) {
        random = new SecureRandom();
        Intent intent = new Intent(this, puzzles[random.nextInt(puzzles.length)]);
        startActivity(intent);
    }

}
