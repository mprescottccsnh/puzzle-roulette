package com.group.cis248.puzzleroulette;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Random;

public class RandomNumberActivity_FG extends AppCompatActivity {
    String LOG = "RandomNumberActivity_FG";
    TextView mSecretNumber;
    TextView mGuessStatus;
    TextView mRandomNumberGuess;
    int myGuessNum;
    final int RAN = 10;

    // generate secret number
    Random secretGenerator = new Random();
    int mySecretNum = secretGenerator.nextInt(RAN);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_number);

        // TextViews
        mSecretNumber =  (TextView) findViewById(R.id.secretNumber);
        mGuessStatus = (TextView) findViewById(R.id.guessStatus);
        mRandomNumberGuess = (TextView) findViewById(R.id.randomNumGuess);

        // Game logic: if user entered a matched number then user win
        mSecretNumber.setText(mySecretNum+""); // CHEAT
    }

    /**
     * This function is called when button "Try Guess" is clicked
     */
    public void onClickTryGuessButton(View v) {
        Random randGenerator = new Random();
        myGuessNum = randGenerator.nextInt(RAN);
        Log.i(LOG, myGuessNum+"---test");

        // display random guess
        mRandomNumberGuess.setText(myGuessNum+"");
    }

    /**
     * This function is called when button "Enter Guess" is clicked
     */
    public void onClickEnterGuess(View v) {
        // let user know the guess result
        if (mySecretNum == myGuessNum) {
            mSecretNumber.setText(mySecretNum+"");
            mGuessStatus.setText("YOU WIN");
            mGuessStatus.setTextColor(Color.BLUE);
        }
        else {
            mGuessStatus.setText("incorrect");
            mGuessStatus.setTextColor(Color.RED);
        }
    }
}
